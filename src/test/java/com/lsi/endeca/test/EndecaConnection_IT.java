package com.lsi.endeca.test;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.endeca.navigation.DimValIdList;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.DimensionList;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERecSearch;
import com.endeca.navigation.ERecSearchList;
import com.endeca.navigation.UrlENEQuery;
import com.lsi.endeca.SearchMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class EndecaConnection_IT {

    private static final Logger logger = Logger.getLogger(EndecaConnection_IT.class);

    @Autowired
    private ENEConnection endecaServerConnection;

    @Test
    public void testEndecaConnection() throws Exception {
	ENEQuery testQuery = new UrlENEQuery(
		"N=0&Ntk=interface&Ntt=tablet&Nty=1&D=tablet&Ntx=mode+matchany&Dx=mode+matchany", "UTF-8");
	ENEQueryResults queryResults = endecaServerConnection.query(testQuery);

	logEndecaQueryStats(testQuery);

	logger.info("This is Rakesh");
	logger.info(ToStringBuilder.reflectionToString(queryResults));
    }

    private void logEndecaQueryStats(ENEQuery testQuery) {
	logger.info("Navigation query type?" + testQuery.containsNavQuery());
	logger.info("Endeca Record query type?:" + testQuery.containsERecQuery());
	logger.info("Endeca Dimension search query type?:" + testQuery.containsDimSearchQuery());
	logger.info("Endeca Aggregated Endeca record query type?:" + testQuery.containsAggrERecQuery());
    }

    @Test
    public void performKeywordSearch() throws Exception {
	final ENEQuery query = new ENEQuery();
        
	final DimValIdList dimValIdList = new DimValIdList("0");
        query.setNavDescriptors(dimValIdList);
        
        query.setNavNumERecs(100);
        query.setNavKeyProperties(ENEQuery.KEY_PROPS_ALL);
        
        final ERecSearchList searches = new ERecSearchList();
	searches.add(new ERecSearch("All", "mirror", SearchMode.ANY.getMode()));
	query.setNavERecSearches(searches);
	//query.setERecSpec("142294CC8B78");
	
        logEndecaQueryStats(query);
       
        
        final ENEQueryResults results = endecaServerConnection.query(query); 
        	
        final StringBuilder builder = new StringBuilder();
        
        final DimensionList dimensions = results.getNavigation()
                .getCompleteDimensions();
 
        for (Object element : dimensions) {
            final Dimension dimension = (Dimension) element;
            builder.append("id : ");
            builder.append(dimension.getId());
            builder.append(" -> name :");
            builder.append(dimension.getName());
            builder.append("\n");
        }
	logger.info(builder.toString());
    }

    private static ENEQuery createKeywordSearchQuery(final String searchTerm) {
	final ENEQuery query = new ENEQuery();
	final DimValIdList dimValIdList = new DimValIdList("0");
	query.setNavDescriptors(dimValIdList);
	final ERecSearchList searches = new ERecSearchList();
	searches.add(new ERecSearch("P_Auction_Id", searchTerm, SearchMode.ANY.getMode()));
	query.setNavNumERecs(100);
	query.setNavKeyProperties(ENEQuery.KEY_PROPS_ALL);
	return query;
    }
}
