package com.lsi.endeca;

public enum SearchMode {

	ALL("mode matchall"),

	ANY("mode matchany"),

	PARTIAL("mode matchpartial"),

	ALLPARTIAL("mode matchallpartial");

	private String searchMode;

	public String getMode() {
		return searchMode;
	}

	private SearchMode(final String searchMode) {
		this.searchMode = searchMode;
	}

}