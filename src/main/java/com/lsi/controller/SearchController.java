package com.lsi.controller;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.AssocDimLocationsList;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValIdList;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecSearch;
import com.endeca.navigation.ERecSearchList;
import com.endeca.navigation.UrlENEQuery;

@Controller
@RequestMapping("/search")
public class SearchController {

	private static final Logger logger = Logger
			.getLogger(SearchController.class);

	@Autowired
	private ENEConnection endecaServerConnection;
	
	private static String NEW_LINE = "</BR>";

	@RequestMapping(method = RequestMethod.GET)
	public String search(HttpServletRequest httpRequest,  ModelMap model)
			throws Exception {

		logger.info("Searching for URL query string: " + httpRequest.getQueryString());
		
		ENEQuery keywordQuery = new UrlENEQuery(httpRequest.getQueryString(),"UTF-8"); //createKeywordQuery(keyWord);
		keywordQuery.setNavKeyProperties(ENEQuery.KEY_PROPS_ALL);

		final ENEQueryResults results = endecaServerConnection
				.query(keywordQuery);
		
		model.addAttribute("searchResults", results);
		captureRecordPropertyKeys(results);
		
		logger.info("ERec properties = " + ToStringBuilder.reflectionToString(results.getNavigation().getKeyProperties()));

		return "searchView";

	}

	private static void captureRecordPropertyKeys(ENEQueryResults eneQueryResults) throws Exception{
		
		Map keyProperties = eneQueryResults.getNavigation().getKeyProperties();
		for(Map.Entry keyPropertyEntry : (Set<Map.Entry>)keyProperties.entrySet()){
			logger.info("Key Property = " + keyPropertyEntry.getKey().toString() + "  value=" + keyPropertyEntry.getValue());
		}
	}
	
	private static ENEQuery createNavigationQuery(final String nValue) {
        final ENEQuery query = new ENEQuery();
        final DimValIdList dimValIdList = new DimValIdList(nValue);
        query.setNavDescriptors(dimValIdList);
        return query;
    }
	
	private static ENEQuery createKeywordSearchQuery(final String searchTerm) {
		final ENEQuery query = new ENEQuery();
		final DimValIdList dimValIdList = new DimValIdList("0");
		query.setNavDescriptors(dimValIdList);
		final ERecSearchList searches = new ERecSearchList();
		searches.add(new ERecSearch("All", searchTerm));
		query.setNavERecSearches(searches);
		return query;
	}
}

/**
		logger.info("Raw result: "
				+ ToStringBuilder.reflectionToString(results.toString()));

		final StringBuilder builder = new StringBuilder();
		builder.append("Found ");
		builder.append(results.getNavigation().getERecs().size());
		builder.append(" results.\n");

		Set<String> dimensions = new HashSet<String>();

		for (Object result : results.getNavigation().getERecs()) {
			final ERec eRec = (ERec) result;
			builder.append(NEW_LINE);
			builder.append("***Record Properties:").append(NEW_LINE);			
			for (Object key : eRec.getProperties().keySet()) {
				builder.append(key);
				builder.append(" >> ");
				builder.append(eRec.getProperties().get(key));				
				builder.append(NEW_LINE);				
			}
			builder.append("***").append(NEW_LINE);
			builder.append("***Record dimensions:").append(NEW_LINE);
			AssocDimLocationsList dims = (AssocDimLocationsList) eRec.getDimValues();
			for (int j = 0; j < dims.size(); j++) {
				// Get individual dimension and loop over its values
				AssocDimLocations dim = (AssocDimLocations) dims.get(j);
				for (int k = 0; k < dim.size(); k++) {
					// Get attributes from a specific dim val
					DimLocation dimLoc = (DimLocation) dim.get(k);
					DimVal dval = dimLoc.getDimValue();
					String dimensionName = dval.getDimensionName();
					long dimensionId = dval.getDimensionId();
					String dimValName = dval.getName();
					long dimValId = dval.getId();

					builder.append(dimValName).append(NEW_LINE);
				}
			}
			builder.append("***");
			builder.append(NEW_LINE);
			builder.append("----------------");
			builder.append(NEW_LINE);
		}

		logger.debug("dimensions: " + dimensions);
		String message = "Search Result: " + builder.toString();

		logger.debug(message);

		model.addAttribute("message", message);
*/
