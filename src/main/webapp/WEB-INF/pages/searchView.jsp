<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.endeca.navigation.*" %>
<%@ page import="com.endeca.logging.*" %>
<%@ page import="org.apache.commons.lang3.builder.ToStringBuilder" %>
<%@ page import="java.util.*" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript" src="../js/jquery-latest.js"></script>
	<script type="text/javascript" src="../js/jquery-ui-latest.js"></script>
	<script type="text/javascript" src="../js/jquery.layout-latest.js"></script>

	<script>
	    $(document).ready(function () {
	        $('body').layout({ applyDefaultStyles: true });
	    });
	</script>
    <title>Endeca Search POC</title>
</head>
<body>
	<%!
		String SEARCH_PAGE_URL = "/endeca-client-poc/views/search";
	%>


	<div class="ui-layout-west">
		<!------------------------------------------------------------------------------->
		<!-- retrieve all Dimensions-->
		
		<table border="1" cellspacing="0" cellpadding="0">	
			<tr bgcolor="gray"><th colspan="2">DIMENSIONS and DIMENSION GROUPS</td></tr>
			
			<%
		
			Navigation nav = ((ENEQueryResults)request.getAttribute("searchResults")).getNavigation();
			
			// Get refinement dimension groups
			DimGroupList refDimensionGroups = nav.getRefinementDimGroups();
		
			// Get descriptor dimensions
			DimensionList descDimensionsNC = nav.getDescriptorDimensions();
		
			// Loop over dimension groups
			for (int i=0; i<refDimensionGroups.size(); i++) {
		
				// Get dimension group object
				DimGroup dg = (DimGroup)refDimensionGroups.get(i);			
		
				//  If group is explictic (not default group), display group
				if (dg.isExplicit()) {
					%>
					<tr><td colspan="2" color="gray"></td></tr>
					<tr><td colspan="2"><font face="arial" size="2" color="#999999"><%= dg.getName() %></font></td></tr>
					<%
				}
		
				// Loop over dimensions in group
				for (int j=0; j<dg.size(); j++) {
		
					// Get dimension object
					Dimension dim = (Dimension)dg.get(j);
		
					// Get root for dimension
					DimVal root = dim.getRoot();
		
					// Get id of root
					long rootId = root.getId();
		
					// Get refinement list for dimension
					DimValList refs = dim.getRefinements();
		
					// Create request to expose dimension values
					UrlGen urlg = new UrlGen(request.getQueryString(), "UTF-8");			
					urlg.removeParam("D");
					urlg.removeParam("Dx");
					urlg.removeParam("sid");
					urlg.removeParam("in_dym");
					urlg.removeParam("in_dim_search");
					urlg.addParam("sid",(String)request.getAttribute("sid"));
					
					// Expand dimension
					if (refs.size() == 0) {
						urlg.addParam("Ne",Long.toString(rootId));
					}
					// Close dimension
					else {
						urlg.removeParam("Ne");
					}			
					String url = SEARCH_PAGE_URL + "?" + urlg;
		
					// Display dimension (open row here, close later)
					%>
					<tr><td colspan="2"><a href="<%= url %>"><font face="arial" size="2" color="#444444"><!--S:CGRP--><%= dim.getName() %><!--E:CGRP--></font></a><%
		
					// Get intermediate list for dimension
					DimValList ints = dim.getIntermediates();
		
					// Loop over intermediate list
					for (int k=0; k < ints.size(); k++) {
		
						// Get intermediate dimension value
						DimVal intermediate = ints.getDimValue(k);
		
						// Display intermediate
						%><font face="arial" size="2" color="#444444"> > <%= intermediate.getName() %></font><%
					}
		
					// Close nav row
					%></td></tr><%
		
					// Loop over refinement list
					for (int k=0; k < refs.size(); k++) {
		
						// Get refinement dimension value
						DimVal ref = refs.getDimValue(k);
		
						// Get properties for refinement value
						PropertyMap pmap = ref.getProperties();
		
						// Get dynamic stats
						String dstats = "";
						if (pmap.get("DGraph.Bins") != null) {
							dstats = " ("+pmap.get("DGraph.Bins")+")";
						}
		
						// Create request to select refinement value
						urlg = new UrlGen(request.getQueryString(), "UTF-8");
		
						// If refinement is navigable, change the Navigation parameter
						if (ref.isNavigable()) {
							urlg.addParam("N",(ENEQueryToolkit.selectRefinement(nav,ref)).toString());
							urlg.addParam("Ne",Long.toString(rootId));
						}
						// If refinement is non-navigable, change only the exposed dimension parameter
						// (Leave the Navigation parameter as is)
						else {
							urlg.addParam("Ne",Long.toString(ref.getId()));
						}
						urlg.removeParam("No");
						urlg.removeParam("Nao");
						urlg.removeParam("Nty");
						urlg.removeParam("D");
						urlg.removeParam("Dx");
						urlg.removeParam("sid");
						urlg.removeParam("in_dym");
						urlg.removeParam("in_dim_search");
						urlg.addParam("sid",(String)request.getAttribute("sid"));
						url = SEARCH_PAGE_URL + "?" + urlg;
		
						String edge = "<!--S:EDGE-->" + ref.getName() + "<!--E:EDGE-->";
						String color = "blue";
						if (ENEQueryToolkit.isImplicitRefinement(dim, ref)) {
							edge = "<del>" + edge + "</del>";
							color = "#444444";
						}
						// Display refinement
						%>
						<tr>
						<td color="gray"></td>
						<td width="100%"><a href="<%= url %>"><font face="arial" size="1" color="<%= color %>"><%= edge %></font></a><font face="arial" size="1" color="gray"><%= dstats %></font></td>
						</tr>
						<%
					}
				}
		
				//  If group is explictic (not default group), display spacer
				if (dg.isExplicit()) {
					%>
					<tr><td colspan="2" color="gray"></td></tr>			
					<%
				}
		
			}			
			
			%>
			<tr><td colspan="2" color="gray"></td></tr>
		</table>
				
	</div>

	<div class="ui-layout-north" align="center">
		<!------------------------------------------------------------------------------->
		<!-- Records keyword search box -->
		<table  border="1" cellspacing="0" cellpadding="0">
			<tr bgcolor="gray" width="100%"> <th> KEYWORD SEARCH BOX </th></tr>
			<tr>
				<td>
				<form action="<%=SEARCH_PAGE_URL %>">
						<input type="hidden" name="N" value="<%= request.getParameter("N") %>"/>
						<% if(request.getParameter("Ne") == null){
						%>
							<input type="hidden" name="Ne" value="2"/>
						<% }else{
						%>
							<input type="hidden" name="Ne" value="<%= request.getParameter("Ne") %>"/>
						<%
							}
						%>
						<select name="Ntk">
						<%
							// Get search key list
							ERecSearchKeyList skl = ((ENEQueryResults)request.getAttribute("searchResults")).getNavigation().getERecSearchKeys();
							
							// Get search group key list
							ERecCompoundSearchKeyList sgkl = ((ENEQueryResults)request.getAttribute("searchResults")).getNavigation().getERecCompoundSearchKeys();
				
							// Loop over valid search group keys
							for (int i=0; i < sgkl.size(); i++) {
					
								// Get specific search group key
								ERecCompoundSearchKey sgkey = sgkl.getKey(i);
					
								// Figure out if search group key is active
								String activeKey = "";
								if (sgkey.isActive()) {
									activeKey = "selected";
								}
					
						%>
								<option value="<%= sgkey.getName() %>" <%= activeKey %> style="color:blue"><%= sgkey.getName() %></option>
						<%
							}
					
							// Loop over valid search keys
							for (int i=0; i < skl.size(); i++) {
					
								// Get specific search key
								ERecSearchKey skey = skl.getKey(i);
					
								// Figure out if search key is active
								String activeKey = "";
								if (skey.isActive()) {
									activeKey = "selected";
								}
								
						%>
								<option value="<%= skey.getName() %>" <%= activeKey %>><%= skey.getName() %></option>
						<%
							}
						
						%>		
						</select>
						<input type="text" name="Ntt" value="<%= request.getParameter("Ntt") %>"/>
						<input type="submit"/>
				</form>
				</td>
			</tr>
		</table>
		
	</div>
	<div class="ui-layout-center">
		<!------------------------------------------------------------------------------->
		<!-- Records : Search results -->
		<table  border="1" cellspacing="0" cellpadding="0">
			<tr bgcolor="gray" width="100%"> <th colspan="2"> RECORDS - SEARCH RESULTS </th></tr>
			<%
				for (Object result : ((ENEQueryResults)request.getAttribute("searchResults")).getNavigation().getERecs()) {
					final ERec eRec = (ERec) result;
			%>
					<tr bgcolor="#404040" colspan="2"><td></br></td></tr>
			<%
					for (Object key : eRec.getProperties().keySet()) {			
			%>
						<tr><td><%= key %></td><td><%=eRec.getProperties().get(key) %></td></tr>
			<%
					}
				}
			%>
		</table>
	</div>
	<div class="ui-layout-south">Endeca POC</div>
	<div class="ui-layout-east">
		<!------------------------------------------------------------------------------->
		<!-- retrieve all endeca record properties that can be used for search keywords-->
		<table  border="1" cellspacing="0" cellpadding="0">
			<tr bgcolor="gray">
				<th width="100%">SEARCH PROPERTY KEYS</th>
			</tr>
		<%
			Map recordPropertiesMap = ((ENEQueryResults)request.getAttribute("searchResults")).getNavigation().getKeyProperties();
			for(Map.Entry keyPropertyEntry : (Set<Map.Entry>)recordPropertiesMap.entrySet()){
		 		KeyProperties propertyKeyValues = (KeyProperties)keyPropertyEntry.getValue();
		    	String propertyKeyValueName = (String)propertyKeyValues.getKey();
		%>
		    	<tr> <td> <%= propertyKeyValueName %>&nbsp; </td> </tr>
		<%
			}
		%>
		</table>
	</div>
</body>
</html>